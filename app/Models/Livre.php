<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Livre extends Model
{
    use HasFactory;

    protected $fillable = [
        "titre",
        "pages",
        "description",
        "categorie_id",
        "image"
    ];

    function categorie():BelongsTo
    {
        return $this->belongsTo(Categorie::class) ;
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Livre;
use Illuminate\Http\Request;

class LivreController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $livres = Livre::paginate(20);
        return view('livres.index', compact('livres'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('livres.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'titre' => 'required|string,max:255',
            'pages' => 'required|integer,gt:0',
            'description' => 'required',
            'categorie_id' => 'required',
            'image'=> 'mime:svg,png,jpeg'
        ]);
        if($request->hasFile("image")) {
            $imagePath = $request->file("image")->store("livre/image","public");
            $validateData["image"] = $imagePath;
        }
        return redirect()->route('livre.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $livre = Livre::find($id);
        return view('livre.edit', compact('livre'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data = $request->validate([
            'titre' => 'required|string',
            'pages' => 'required|integer',
            'description' => 'required|string',
        ]);

        $livre= Livre::find($id);
        // $livre->update();

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Livre::find($id)->delete();
        return redirect()->back();
    }
}

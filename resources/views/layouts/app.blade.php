<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>@yield('title')</title>
    <style>
        nav {
            display: flex;
            align-items: center;
            justify-content: center;
            background-color: rgb(52, 148, 199);
            padding: 2rem;
            font-size: 1.5rem;
            /* justify-items: space-around; */
        }

        nav div {
            margin: 1.5rem;
        }
    </style>
</head>

<body>
    <nav>
        <div>
            <a class="link" href={{ route('livres.index') }}>Home</a>
        </div>
        {{-- <div><a href="">Edit</a></div> --}}
    </nav>

    @yield('content')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
</body>

</html>

@extends('layouts.app')
@section('title','livres')
@section('content')
    <a href={{route('livres.create')}}>Ajouter un livre</a>
<table class="table">
    <thead>
        <th>titre</th>
        <th>pages</th>
        <th>desctiprion</th>
        <th>categorie</th>
        <th colspan="2">Action</th>
    </thead>
    <tbody>
        @foreach ($livres as $livre)
            <tr>
                <td>{{$livre->titre}}</td>
                <td>{{$livre->pages}}</td>
                <td>{{$livre->description}}</td>
                <td>{{$livre->categorie->nom}}</td>
                <td><a href={{route('livres.edit',$livre->id)}} class='btn btn-primary'>editer</a></td>
                <td><a href={{route('livres.destroy',$livre->id)}} class='btn btn-primary'>supprimer</a></td>
            </tr>
        @endforeach

    </tbody>
</table>

@endsection